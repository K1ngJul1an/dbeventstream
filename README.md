# DB Event Stream
## Logical Decoding for Spring

### What is Logical Decoding ?

Logical Decoding is a feature of PostgreSQL which facilitates streaming the changes to a database to 
the user. There can be more than one stream of changes for a given database and each stream is bound 
to a replication slot. Replication slots have a unique name in the database cluster and exist 
independently of connections and are crash safe. Slots send the changes to a plugin. A plugin is a 
small library that can be configured to filter the tables to be decoded as well as format the output.

What this means to the consumer of the replication stream is that if the database is shut down any 
unread changes will be available to be read when the server comes back up. An artifact of this is 
that if the client dies or is shut down without reading all of the changes they can be read when the
client resumes.

### Change Data Capture (CDC)

 - Logical Decoding enables CDC without using tricks such as triggers, log tables, 
 timestamps, polling, etc. which all incur some form of performance overhead. As well as making 
 it much more robust as the server manages this process as part of the Write Ahead Logs (WAL).
 
 - Propagating changes via message queues: There is a growing enterprise pattern where incoming data
 is distributed to reporting, fullfillment, etc via message queues. 
 
 - Replicating data to another database: Fine grained replication. Replicate only as much of the 
 database as necessary. Facilicates replicating to a disparate database such as PostgreSQL to MySql 
 for example. It would also be possible to filter the data to remove sensitive data. 


### Prerequisites

- PostgreSQL version 9.5 or newer.
- PostgreSQL needs to be configured properly to enable Logical Decoding.
- a user with replication privileges

- postgresql.conf file
  - max_replication_slots > 0
  - max_wal_senders > 0
  - wal_level at least at level logical
  
- pg_hba.conf requires an entry for the replication user
  - host replication <repuser> ip/mask md5 for example
  
  
### Using this in spring

This library provides an annotation to annotate a method in a class which will receive changes from 
the database.

Note the method returns boolean. Return true to tell the encoder that you have read the change.
If you return false the change will be sent to you again.

Example:

```java
package org.postgresql;

import org.postgresql.annotation.EnablePostgreSQL;
import org.postgresql.annotation.PostgresLogicalDecoderListener;


@EnablePostgreSQL
public class ChangeDataCapture {

  @PostgresLogicalDecoderListener(slotName = "test", encoder = "wal2jsonTest")
  public boolean listen( String transaction) {

    System.out.println( transaction );
    return true;
  }


}
```

As the encoder is Wal2Json the listen function will receive a JSON string which will look something like

```JSON
{
	"xid": 84401,
	"change": [
		{
			"kind": "insert",
			"schema": "public",
			"table": "customers",
			"columnnames": [
				"id",
				"first_name",
				"last_name"
			],
			"columntypes": [
				"int4",
				"varchar",
				"varchar"
			],
			"columnvalues": [
				421,
				"John",
				"Woo"
			]
		},
		{
			"kind": "insert",
			"schema": "public",
			"table": "customers",
			"columnnames": [
				"id",
				"first_name",
				"last_name"
			],
			"columntypes": [
				"int4",
				"varchar",
				"varchar"
			],
			"columnvalues": [
				422,
				"Jeff",
				"Dean"
			]
		},
		{
			"kind": "insert",
			"schema": "public",
			"table": "customers",
			"columnnames": [
				"id",
				"first_name",
				"last_name"
			],
			"columntypes": [
				"int4",
				"varchar",
				"varchar"
			],
			"columnvalues": [
				423,
				"Josh",
				"Bloch"
			]
		},
		{
			"kind": "insert",
			"schema": "public",
			"table": "customers",
			"columnnames": [
				"id",
				"first_name",
				"last_name"
			],
			"columntypes": [
				"int4",
				"varchar",
				"varchar"
			],
			"columnvalues": [
				424,
				"Josh",
				"Long"
			]
		}
	]
}
```

There are number of beans that need to be configure to make this work: see DataConfig.java

```java
package org.postgresql;

import org.postgresql.encoder.Decoder;
import org.postgresql.encoder.Wal2JSON;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.Properties;

import javax.sql.DataSource;

@Configuration
public class DataConfig {

  /**
   * This is a normal data source for doing regular CRUD operations on the databbase
   * @return
   */
  @Bean
  public DataSource postgresqlDataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUrl("jdbc:postgresql://localhost:5432/test");
    dataSource.setUsername("test");
    dataSource.setPassword("test");
    return dataSource;
  }

  /**
   * provide spring with a jdbcTemplate to do CRUD operations. Make sure it uses the above
   * datasource using the @Qualifier annotation.
   *
   * @param dataSource
   * @return
   */
  @Bean JdbcTemplate jdbcTemplate(@Qualifier("postgresqlDataSource") DataSource dataSource) {
    return new JdbcTemplate(dataSource);
  }

  /**
   * create a replicationDataSource for Logical Decoding
   *
   * Note we have some special properties.
   * PREFER_QUERY_MODE:simple
   * REPLICATION:databse
   *
   * These are required for Logical Decoding to work
   * 
   * @return
   */
  @Bean
  @Scope("singleton")
  public DataSource  replicationDataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUrl("jdbc:postgresql://localhost:5432/test");
    dataSource.setUsername("davec");
    dataSource.setPassword("");
    Properties properties = new Properties();
    PGProperty.ASSUME_MIN_SERVER_VERSION.set(properties, "9.4");
    PGProperty.REPLICATION.set(properties, "database");
    PGProperty.PREFER_QUERY_MODE.set(properties, "simple");
    dataSource.setConnectionProperties(properties);
    return dataSource;
  }

  /**
   * create a PGTemplate bean which has some extra methods specific to PostgreSQL
   * ensure that it uses replicationDataSource using @Qualifier
   * @param dataSource
   * @return
   */
  @Bean
  public PGTemplate pgTemplate(@Qualifier("replicationDataSource") DataSource dataSource) {
    return new PGTemplateImplementation(dataSource);

  }

  /**
   * create a bean that will handle the changes
   * @return
   */
  @Bean
  public ChangeDataCapture postgresLogicalDecoderHandler() {
    return new ChangeDataCapture();
  }

  /**
   * see the documentation for wal2json for all of the options
   * available.
   * we also need a encoder to associate with the slot.
   * @return
   */
  @Bean
  public Decoder wal2jsonTest() {
    Decoder encoder = new Wal2JSON().
        setXids(true);
    return  encoder;
  }
}
```




