package org.postgresql;


public interface LogicalDecoder <T> {
  T decode(byte[] buf, int offset, int length);
}
