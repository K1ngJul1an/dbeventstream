package org.postgresql;

import org.postgresql.replication.LogSequenceNumber;
import org.postgresql.replication.PGReplicationStream;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.JdbcOperations;

import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

public interface PGTemplate extends InitializingBean, JdbcOperations {

  /**
   * stop a slot from replicating.
   *
   * @param slotName
   * @return
   */
  boolean stopSlot(String slotName) throws SQLException;

  /**
   * test to see if a slot exists
   * @param slotName
   * @return
   * @throws SQLException
   */
  boolean slotExists(String slotName) throws SQLException;
  /**
   *
   * Drops an already existing slot.
   * There cannot be two slots with the same name
   *
   * @param slotName
   * @return
   * @throws SQLException
   * @throws InterruptedException
   * @throws TimeoutException
   */
  boolean dropReplicationSlot(String slotName) throws SQLException, InterruptedException,  TimeoutException;

  /**
   * Create a slot and associate a plugin with it.
   * @param slotName
   * @param outputPlugin
   * @return
   * @throws SQLException
   */
  LogSequenceNumber createLogicalReplicationSlot(String slotName, String outputPlugin) throws
      SQLException;

  /**
   *
   * @param slotName
   * @return
   * @throws SQLException
   */
  boolean isReplicationSlotActive(String slotName) throws SQLException;

  /**
   *
   * @param slotName
   * @param lsn
   * @param slotOptions
   * @return
   * @throws SQLException
   */
  PGReplicationStream getReplicationStream(String slotName, LogSequenceNumber lsn,
      Properties slotOptions) throws SQLException;

  /**
   *
   * @param stream
   * @param logicalDecoder
   * @param <T>
   * @return
   * @throws SQLException
   */
  <T> T readStream(PGReplicationStream stream,
      LogicalDecoder<T> logicalDecoder) throws SQLException;

  /**
   *
   * @param publication
   * @throws SQLException
   */
  void dropPublication(String publication) throws SQLException;

  /**
   *
   * @param publication
   * @throws SQLException
   */
  void createPublication(String publication) throws SQLException;
}
