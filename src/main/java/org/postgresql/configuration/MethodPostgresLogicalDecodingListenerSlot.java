package org.postgresql.configuration;

import org.postgresql.listener.PostgresLogicalDecoderErrorHandler;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanExpressionContext;
import org.springframework.beans.factory.config.BeanExpressionResolver;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.BeanResolver;

import java.lang.reflect.Method;

public class MethodPostgresLogicalDecodingListenerSlot <K,V> implements BeanFactoryAware,
    InitializingBean {

  private Object bean;

  private Method method;

  private BeanFactory beanFactory;

  private BeanExpressionResolver resolver;

  private BeanExpressionContext expressionContext;

  private BeanResolver beanResolver;

  private PostgresLogicalDecoderErrorHandler errorHandler;

  public void setBean(Object bean) {
    this.bean = bean;
  }

  public void setMethod(Method method) {
    this.method = method;
  }

  public java.lang.reflect.Method getMethod() {
    return method;
  }

  @Override
  public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
    this.beanFactory = beanFactory;
    if (beanFactory instanceof ConfigurableListableBeanFactory) {
      this.resolver = ((ConfigurableListableBeanFactory) beanFactory).getBeanExpressionResolver();
      this.expressionContext = new BeanExpressionContext((ConfigurableListableBeanFactory) beanFactory, null);
    }
    this.beanResolver = new BeanFactoryResolver(beanFactory);
  }

  public BeanFactory getBeanFactory() {
    return this.beanFactory;
  }

  protected BeanExpressionResolver getResolver() {
    return this.resolver;
  }

  protected BeanExpressionContext getBeanExpressionContext() {
    return this.expressionContext;
  }

  protected BeanResolver getBeanResolver() {
    return this.beanResolver;
  }

  public void setErrorHandler(PostgresLogicalDecoderErrorHandler errorHandler){
    this.errorHandler = errorHandler;
  }
  public PostgresLogicalDecoderErrorHandler getErrorHandler() {
    return errorHandler;
  }
  /**
   * Invoked by a BeanFactory after it has set all bean properties supplied (and satisfied
   * BeanFactoryAware and ApplicationContextAware).
   * <p>This method allows the bean instance to perform initialization only
   * possible when all bean properties have been set and to throw an exception in the event of
   * misconfiguration.
   *
   * @throws Exception in the event of misconfiguration (such as failure to set an essential
   *                   property) or if initialization fails.
   */
  @Override
  public void afterPropertiesSet() throws Exception {

  }
}
