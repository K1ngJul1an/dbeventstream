package org.postgresql.encoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class Wal2JSON implements Encoder {

  private static final String INCLUDE_XIDS      = "include-xids";
  private static final String INCLUDE_TIMESTAMP = "include-timestamp";
  private static final String INCLUDE_SCHEMAS   = "include-schemas";
  private static final String INCLUDE_TYPES     = "include-types";
  private static final String INCLUDE_TYPMOD    = "include-typmod";
  private static final String INCLUDE_TYPE_OIDS = "include-type-oids";
  private static final String INCLUDE_NOT_NULL  = "include-not-null";
  private static final String PRETTY_PRINT      = "pretty_print";
  private static final String WRITE_IN_CHUNKS   = "write_in_chunks";
  private static final String INCLUDE_LSN       = "include-lsn";
  private static final String INCLUDE_UNCHANGED_TOAST = "include-unchanged-toast";
  private static final String FILTER_TABLES     = "filter_tables";
  private static final String ADD_TABLES        = "add_tables";





  private final List<String> filterTables,
               addTables;


  private Properties properties = new Properties();

  public Wal2JSON() {
    filterTables = new ArrayList<String>();
    addTables = new ArrayList<String>();
  }

  /**
   * include xids in each changeset, default is false
   * @param xids
   */
  public Wal2JSON setXids(boolean xids) {
    if (xids == true ){
      properties.setProperty(INCLUDE_XIDS,"true");
    } else {
      properties.remove(INCLUDE_XIDS);
    }
    return this;
  }

  /**
   * include timestamps in each changeset, default is false
   * @param timestamp
   */
  public Wal2JSON setTimestamp(boolean timestamp) {
    if (timestamp == true ){
      properties.setProperty(INCLUDE_TIMESTAMP,"true");
    } else {
      properties.remove(INCLUDE_TIMESTAMP);
    }
    return this;
  }

  /**
   * add schema to each change, default is true.
   * @param schemas
   */
  public Wal2JSON setSchemas(boolean schemas) {
    if (schemas == false ) {
      properties.setProperty(INCLUDE_SCHEMAS, "false");
    } else {
      properties.remove(INCLUDE_SCHEMAS);
    }
    return this;
  }

  /**
   * add type to each change, default is true.
   * @param types
   */
  public Wal2JSON setTypes(boolean types) {
    if (types == false) {
      properties.setProperty(INCLUDE_TYPES, "false");
    } else {
      properties.remove(INCLUDE_TYPES);
    }
    return this;
  }

  /**
   * add modifier to type if they have it (eg. varchar(20) instead of varchar) default true
   * @param typmod
   */
  public Wal2JSON setTypmod(boolean typmod) {
    if (typmod == false) {
      properties.setProperty(INCLUDE_TYPMOD, "false");
    } else {
      properties.remove(INCLUDE_TYPMOD);
    }
    return this;
  }

  /**
   * add type oids, default is false
   * @param typeOids
   */
  public Wal2JSON setTypeOids(boolean typeOids) {
    if ( typeOids == true ) {
      properties.setProperty( INCLUDE_TYPE_OIDS, "true");
    } else {
      properties.remove(INCLUDE_TYPE_OIDS);
    }
    return this;
  }

  /**
   * add not null information as columnoptionals, default is false.
   * @param notNull
   */
  public Wal2JSON setNotNull(boolean notNull) {
    if ( notNull == true ){
      properties.setProperty( INCLUDE_NOT_NULL, "true");
    } else {
      properties.remove( INCLUDE_NOT_NULL );
    }
    return this;
  }

  /**
   * add spaces and indentation, default is false
   * @param prettyPrint
   */
  public Wal2JSON setPrettyPrint(boolean prettyPrint) {
    if ( true  == prettyPrint ) {
      properties.setProperty( PRETTY_PRINT, "true");
    } else {
      properties.remove( PRETTY_PRINT );
    }
    return this;
  }

  /**
   * write after each change instead of every changeset, default is false
   * @param writeInChunks
   */
  public Wal2JSON setWriteInChunks(boolean writeInChunks) {
    if ( true == writeInChunks ){
      properties.setProperty(WRITE_IN_CHUNKS, "true");
    } else {
      properties.remove( WRITE_IN_CHUNKS );
    }
    return this;
  }

  /**
   * add nextlsn to each changeset default is false
   * @param lsn
   */
  public Wal2JSON setLsn(boolean lsn) {
    if ( true == lsn ){
      properties.setProperty(INCLUDE_LSN, "true");
    } else {
      properties.remove(INCLUDE_LSN);
    }
    return this;
  }

  /**
   * add TOAST value even if it was not modified. Since TOAST values
   * are usually large, this option could save IO and bandwidth if
   * it is disabled default true
   * @param unchangedToast
   */
  public Wal2JSON setUnchangedToast(boolean unchangedToast) {
    if ( false == unchangedToast ){
      properties.setProperty(INCLUDE_UNCHANGED_TOAST,"false");
    } else {
      properties.remove( INCLUDE_UNCHANGED_TOAST );
    }
    return this;
  }

  /**
   * exclude rows from specified tables, default is empty
   * tables should be fully qualified including schemas. There are wildcards
   * for schemas and tables *.foo is all tables named foo in all schemas
   * foo.* is all tables in the schema foo.
   * Special characters (space, single quote, comma, period, asterisk)
   * must be escaped with backslash. Schema and table are case-sensitive.
   * Table "public"."Foo bar" should be specified as public.Foo\ bar.
   * @param tableName
   */
  public Wal2JSON addTable(String tableName) {
    addTables.add(tableName);
    return this;

  }

  /**
   * include only rows from the specified tables, default is all tables
   * see addTable for details on table names
   * @param filter
   */
  public Wal2JSON addFilter(String filter) {
    filterTables.add(filter);
    return this;
  }

  @Override
  public Properties getSlotOptions() {
    if (!filterTables.isEmpty()) {
      properties.setProperty(FILTER_TABLES,filterTables.toString());
    }
    if (!addTables.isEmpty()){
      properties.setProperty(ADD_TABLES,addTables.toString());
    }
    return properties;
  }

  /**
   *
   * @return name of plugin
   */
  @Override
  public String getName() {
    return "wal2json";
  }
}
