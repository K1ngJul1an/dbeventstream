package org.postgresql;

import org.postgresql.annotation.EnablePostgreSQL;
import org.postgresql.annotation.PostgresLogicalDecoderListener;


@EnablePostgreSQL
public class ChangeDataCapture {

  @PostgresLogicalDecoderListener(slotName = "test", decoder = "wal2jsonTest")
  public boolean listen( String transaction) {

    System.out.println( transaction );
    return true;
  }


}