package org.postgresql;

import org.postgresql.encoder.Encoder;
import org.postgresql.encoder.Wal2JSON;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.Properties;

import javax.sql.DataSource;

@Configuration
public class DataConfig {

  /**
   * This is a normal data source for doing regular CRUD operations on the databbase
   * @return
   */
  @Bean
  public DataSource postgresqlDataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUrl("jdbc:postgresql://localhost:5432/test");
    dataSource.setUsername("test");
    dataSource.setPassword("test");
    return dataSource;
  }

  /**
   * provide spring with a jdbcTemplate to do CRUD operations. Make sure it uses the above
   * datasource using the @Qualifier annotation.
   *
   * @param dataSource
   * @return
   */
  @Bean JdbcTemplate jdbcTemplate(@Qualifier("postgresqlDataSource") DataSource dataSource) {
    return new JdbcTemplate(dataSource);
  }

  /**
   * create a replicationDataSource for Logical Decoding
   *
   * Note we have some special properties.
   * PREFER_QUERY_MODE:simple
   * REPLICATION:databse
   *
   * These are required for Logical Decoding to work
   *
   * @return
   */
  @Bean
  @Scope("singleton")
  public DataSource  replicationDataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUrl("jdbc:postgresql://localhost:5432/test");
    dataSource.setUsername("davec");
    dataSource.setPassword("");
    Properties properties = new Properties();
    PGProperty.ASSUME_MIN_SERVER_VERSION.set(properties, "9.4");
    PGProperty.REPLICATION.set(properties, "database");
    PGProperty.PREFER_QUERY_MODE.set(properties, "simple");
    dataSource.setConnectionProperties(properties);
    return dataSource;
  }

  /**
   * create a PGTemplate bean which has some extra methods specific to PostgreSQL
   * ensure that it uses replicationDataSource using @Qualifier
   * @param dataSource
   * @return
   */
  @Bean
  public PGTemplate pgTemplate(@Qualifier("replicationDataSource") DataSource dataSource) {
    return new PGTemplateImplementation(dataSource);

  }

  /**
   * create a bean that will handle the changes
   * @return
   */
  @Bean
  public ChangeDataCapture postgresLogicalDecoderHandler() {
    return new ChangeDataCapture();
  }

  /**
   * see the documentation for wal2json for all of the options
   * available.
   * we also need a decoder to associate with the slot.
   * @return
   */
  @Bean
  public Encoder wal2jsonTest() {
    Encoder encoder = new Wal2JSON().
        setXids(true);
    return encoder;
  }
}
